﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using covid19_pcr_test_management_system.Web.DTOs;
using covid19_pcr_test_management_system.Web.Data;
using covid19_pcr_test_management_system.Web.Interfaces;
using Microsoft.Extensions.Options;
using covid19_pcr_test_management_system.Web.Helpers;
using Newtonsoft.Json;
using RestSharp;

namespace covid19_pcr_test_management_system.Web.Controllers
{
    public class ReportsController : Controller
    {
        private readonly IHttpUtils _httpUtils;
        private readonly AppSettings _appSettings;       

        public ReportsController(IHttpUtils httpUtils, IOptions<AppSettings> appSettings)
        {
            _httpUtils = httpUtils;
            _appSettings = appSettings.Value;
        }
        
        public async Task<IActionResult> BookingReport()
        {
            var resp = await _httpUtils.Get(_appSettings.BookingReportEndpoint);
            if (resp.code == System.Net.HttpStatusCode.OK)
            {
                var list = JsonConvert.DeserializeObject<List<BookingReportDTO>>(resp.respPayload);
                return View(list);
            }

            return View(new List<BookingReportDTO>());
        }

        public async Task<IActionResult> BookingSlotReport()
        {
            var resp = await _httpUtils.Get(_appSettings.BookingSlotReportEndpoint);
            if (resp.code == System.Net.HttpStatusCode.OK)
            {
                var list = JsonConvert.DeserializeObject<List<BookingSlotReportDTO>>(resp.respPayload);
                return View(list);
            }

            return View(new List<BookingSlotReportDTO>());
        }

        public async Task<IActionResult> TestReport()
        {
            var resp = await _httpUtils.Get(_appSettings.TestReportEndpoint);
            if (resp.code == System.Net.HttpStatusCode.OK)
            {
                var list = JsonConvert.DeserializeObject<List<TestReportDTO>>(resp.respPayload);
                return View(list);
            }

            return View(new List<TestReportDTO>());
        }
    }
}
