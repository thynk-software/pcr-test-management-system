﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using covid19_pcr_test_management_system.Web.DTOs;
using covid19_pcr_test_management_system.Web.Data;
using covid19_pcr_test_management_system.Web.Interfaces;
using Microsoft.Extensions.Options;
using covid19_pcr_test_management_system.Web.Helpers;
using Newtonsoft.Json;
using RestSharp;

namespace covid19_pcr_test_management_system.Web.Controllers
{
    public class AdministratorController : Controller
    {
        private readonly IHttpUtils _httpUtils;
        private readonly AppSettings _appSettings;       

        public AdministratorController(IHttpUtils httpUtils, IOptions<AppSettings> appSettings)
        {
            _httpUtils = httpUtils;
            _appSettings = appSettings.Value;
        }

        //// GET: BookingSlot
        //public async Task<IActionResult> Index()
        //{
        //    var resp = await _httpUtils.Get(_appSettings.BookingSlotEndPoint);
        //    if (resp.code == System.Net.HttpStatusCode.OK)
        //    {
        //        var list = JsonConvert.DeserializeObject<List<BookingSlot>>(resp.respPayload);
        //        return View(list);
        //    }

        //    return View(new List<BookingSlotDTO>());
        //}        
 
        // GET: BookingSlot/Create
        [HttpGet]
        public async Task<IActionResult> AddBookingSlot()
        {            
            var resp = await _httpUtils.Get(_appSettings.LocationEndpoint);
            if (resp.code == System.Net.HttpStatusCode.OK)
            {
                ViewData["LocationList"] = JsonConvert.DeserializeObject<List<LocationDTO>>(resp.respPayload);
                return View();
            }

            return View();
        }

        // POST: BookingSlot/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddBookingSlot([Bind("Date,LocationId,Status")] BookingSlotDTO bookingSlotDTO)
        {
            if (ModelState.IsValid)
            {
                var resp = await _httpUtils.Post(_appSettings.BookingSlotEndPoint, JsonConvert.SerializeObject(bookingSlotDTO), Method.POST);
                if (resp.code == System.Net.HttpStatusCode.Created)
                {
                    TempData["response"] = "Booking Created Successfully";
                    return RedirectToAction("AddBookingSlot");
                }
            }

            TempData["response"] = "An Error Occured";
            return View(bookingSlotDTO);
        }

        [HttpGet]
        public IActionResult AddLocation()
        {
            return View();
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddLocation([Bind("Name,Address")] LocationDTO locationDTO)
        {
            if (ModelState.IsValid)
            {
                var resp = await _httpUtils.Post(_appSettings.LocationEndpoint, JsonConvert.SerializeObject(locationDTO), Method.POST);
                if (resp.code == System.Net.HttpStatusCode.Created)
                {
                    TempData["response"] = "Location Added Successfully";
                    return RedirectToAction("Index");
                }
            }

            TempData["response"] = "An Error Occured";
            return View(locationDTO);
        }
    }
}
