﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using covid19_pcr_test_management_system.Web.DTOs;
using covid19_pcr_test_management_system.Web.Data;
using covid19_pcr_test_management_system.Web.Interfaces;
using covid19_pcr_test_management_system.Web.Helpers;
using Newtonsoft.Json;
using RestSharp;
using Microsoft.Extensions.Options;

namespace covid19_pcr_test_management_system.Web.Controllers
{
    public class IndividualController : Controller
    {
        private readonly IHttpUtils _httpUtils; 
        private readonly AppSettings _appSettings;
        public IndividualController(IHttpUtils httpUtils, IOptions<AppSettings> appSettings)
        {
            _httpUtils = httpUtils;
            _appSettings = appSettings.Value;
        }

        //// GET: Booking
        public async Task<IActionResult> Index()
        {
            var resp = await _httpUtils.Get(_appSettings.BookingReportEndpoint);
            if (resp.code == System.Net.HttpStatusCode.OK)
            {
                var list = JsonConvert.DeserializeObject<List<BookingReportDTO>>(resp.respPayload);
                return View(list);
            }

            return View(new List<BookingReportDTO>());
        }

        // GET: Booking/Create
        [HttpGet]
        public async Task<IActionResult> BookTest()
        {            
            var resp = await _httpUtils.Get(_appSettings.BookingSlotReportEndpoint);
            if (resp.code == System.Net.HttpStatusCode.OK)
            {
                ViewData["BookingSlotList"] = JsonConvert.DeserializeObject<List<BookingSlotReportDTO>>(resp.respPayload);
                return View();
            }

            return View();
        }
      
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> BookTest([Bind("FullName,Phone,TestType,BookingSlotId,BookingStatus")] BookingDTO bookingDTO)
        {
            if (ModelState.IsValid)
            {
                var resp =  await _httpUtils.Post(_appSettings.BookingEndPoint, JsonConvert.SerializeObject(bookingDTO), Method.POST);
                if (resp.code == System.Net.HttpStatusCode.Created)
                {
                    TempData["response"] = "Booking Created Successfully";
                    return RedirectToAction("Index");
                }
            }

            TempData["response"] = "An Error Occured";
            return RedirectToAction("BookTest");
        }

        // GET: Booking/Edit/5
        public async Task<IActionResult> CancelBooking(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            BookingDTO bookingDTO = null;

            var resp = await _httpUtils.Get(_appSettings.BookingEndPoint + "/" + id);
            if (resp.code == System.Net.HttpStatusCode.OK)
            {
                bookingDTO = JsonConvert.DeserializeObject<BookingDTO>(resp.respPayload);
            }
            
            if (bookingDTO == null)
            {
                return NotFound();
            }
            return View(bookingDTO);
        }

        // POST: Booking/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CancelBooking(int id, [Bind("Id")] BookingDTO bookingDTO)
        {
            if (id != bookingDTO.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                var resp = await _httpUtils.Post(_appSettings.BookingEndPoint + "/" + bookingDTO.Id, null, Method.DELETE);
                if (resp.code == System.Net.HttpStatusCode.OK)
                {
                    return View();
                }
            }

            return RedirectToAction("Index");
        }      
    }
}
