﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using covid19_pcr_test_management_system.Web.DTOs;
using covid19_pcr_test_management_system.Web.Data;
using covid19_pcr_test_management_system.Web.Helpers;
using covid19_pcr_test_management_system.Web.Interfaces;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using RestSharp;

namespace covid19_pcr_test_management_system.Web.Controllers
{
    public class LabAdminController : Controller
    {
        private readonly IHttpUtils _httpUtils;
        private readonly AppSettings _appSettings;
        public LabAdminController(IHttpUtils httpUtils, IOptions<AppSettings> appSettings)
        {
            _httpUtils = httpUtils;
            _appSettings = appSettings.Value;
        }
       
        //// GET: TestResult
        //public async Task<IActionResult> Index()
        //{
        //    var resp = await _httpUtils.Get(_appSettings.BookingEndPoint);
        //    if (resp.code == System.Net.HttpStatusCode.OK)
        //    {
        //        var list = JsonConvert.DeserializeObject<List<TestResultDTO>>(resp.respPayload);
        //        return View(list);
        //    }

        //    return View(new List<TestResultDTO>());
        //}

        // GET: TestResult/Create
        public async Task<IActionResult> SetTestResult()
        {
            var resp = await _httpUtils.Get(_appSettings.BookingEndPoint);
            if (resp.code == System.Net.HttpStatusCode.OK)
            {
                var list = JsonConvert.DeserializeObject<List<BookingDTO>>(resp.respPayload);
                return View(list);
            }

            return View(new List<BookingDTO>());            
        }

        // POST: TestResult/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SetTestResult([Bind("BookingId,Status")] TestResultDTO testResultDTO)
        {
            if (ModelState.IsValid)
            {
                var resp = await _httpUtils.Post(_appSettings.TestResultEndpoint, JsonConvert.SerializeObject(testResultDTO), Method.POST);
                if (resp.code == System.Net.HttpStatusCode.Created)
                {
                    TempData["response"] = "Booking Created Successfully";
                    return RedirectToAction("Index");
                }
            }

            TempData["response"] = "An Error Occured";
            return RedirectToAction("BookTest");
        }
  }
}
