namespace covid19_pcr_test_management_system.Web.Helpers
{
    public class AppSettings
    {
        public string APIBaseURL { get; set; }

        private string _BookingEndpoint;
        public string BookingEndPoint { get { return $"{APIBaseURL}{_BookingEndpoint}"; } set { _BookingEndpoint = value; } }
       
        private string _BookingSlotEndpoint;
        public string BookingSlotEndPoint { get { return $"{APIBaseURL}{_BookingSlotEndpoint}"; } set { _BookingSlotEndpoint = value; } }
       
        private string _LocationEndpoint;
        public string LocationEndpoint { get { return $"{APIBaseURL}{_LocationEndpoint}"; } set { _LocationEndpoint = value; } }
       
        private string _TestResultEndpoint;
        public string TestResultEndpoint { get { return $"{APIBaseURL}{_TestResultEndpoint}"; } set { _TestResultEndpoint = value; } }

        private string _BookingReportEndpoint;
        public string BookingReportEndpoint { get { return $"{APIBaseURL}{_BookingReportEndpoint}"; } set { _BookingReportEndpoint = value; } }

        private string _BookingSlotReportEndpoint;
        public string BookingSlotReportEndpoint { get { return $"{APIBaseURL}{_BookingSlotReportEndpoint}"; } set { _BookingSlotReportEndpoint = value; } }

        private string _TestReportEndpoint;
        public string TestReportEndpoint { get { return $"{APIBaseURL}{_TestReportEndpoint}"; } set { _TestReportEndpoint = value; } }
    }
}