﻿using covid19_pcr_test_management_system.Web.Interfaces;
using RestSharp;
using System;
using System.Net;
using System.Threading.Tasks;

namespace covid19_pcr_test_management_system.Web.Helpers
{
    public class HTTPUtils: IHttpUtils
    {
        public async Task<(HttpStatusCode code, string respPayload)> Get(string url)
        {
            string jsonResp = "";
            try
            {
                var client = new RestClient(url);
                client.Timeout = -1;
                var request = new RestRequest(Method.GET);
                request.AddHeader("Content-Type", "application/json");
               
                IRestResponse response = await client.ExecuteAsync(request);
                return (response.StatusCode, response.Content);
            }
            catch (Exception ex)
            {
                return (HttpStatusCode.InternalServerError, ex.ToString());
            }
        }

        public async Task<(HttpStatusCode code, string message)> Post(string url, string payload, Method method)
        {
            string jsonResp = "";
            try
            {
                var client = new RestClient(url);
                client.Timeout = -1;
                var request = new RestRequest(method);
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", payload, ParameterType.RequestBody);
               
                IRestResponse response = await client.ExecuteAsync(request);

                return (response.StatusCode, response.Content);
            }
            catch (Exception ex)
            {
                return (HttpStatusCode.InternalServerError, ex.ToString());
            }
        }
    }
}
