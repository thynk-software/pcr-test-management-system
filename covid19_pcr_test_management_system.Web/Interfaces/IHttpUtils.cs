﻿using RestSharp;
using System.Net;
using System.Threading.Tasks;

namespace covid19_pcr_test_management_system.Web.Interfaces
{
    public interface IHttpUtils
    {
        Task<(HttpStatusCode code, string respPayload)> Get(string url);
        Task<(HttpStatusCode code, string message)> Post(string url, string payload, Method method);
    }
}
