﻿using covid19_pcr_test_management_system.Web.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace covid19_pcr_test_management_system.Web.DTOs
{
    public class TestResultDTO
    {
        public int Id { get; set; }
        public int BookingId { get; set; }
        public TESTSTATUS Status { get; set; }
    }
}
