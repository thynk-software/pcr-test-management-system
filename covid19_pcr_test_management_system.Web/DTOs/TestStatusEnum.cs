﻿using System;
using System.Collections.Generic;
using System.Text;

namespace covid19_pcr_test_management_system.Web.Enums
{
    public enum TESTSTATUS
    {
        POSITIVE,
        NEGATIVE
    }
}
