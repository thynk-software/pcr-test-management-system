﻿using covid19_pcr_test_management_system.Web.Enums;

namespace covid19_pcr_test_management_system.Web.DTOs
{
    public class TestReportDTO
    {
        public int Id { get; set; }
        public int BookingId { get; set; }
        public BookingReportDTO Booking { get; set; }
        public TESTSTATUS Status { get; set; }
    }
}
