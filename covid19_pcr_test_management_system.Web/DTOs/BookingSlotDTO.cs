﻿
using covid19_pcr_test_management_system.Web.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace covid19_pcr_test_management_system.Web.DTOs
{
    public class BookingSlotDTO
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public int LocationId { get; set; }
        public SLOTSTATUS Status { get; set; }
    }
}
