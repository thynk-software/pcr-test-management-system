﻿


using covid19_pcr_test_management_system.Web.Enums;

namespace covid19_pcr_test_management_system.Web.DTOs
{
    public class BookingDTO
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public TESTTYPE TestType { get; set; }
        public int BookingSlotId { get; set; }
        public BOOKINGSTATUS BookingStatus { get; set; }
    }
}
