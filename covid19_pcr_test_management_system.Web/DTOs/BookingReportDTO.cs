﻿using covid19_pcr_test_management_system.Web.Enums;

namespace covid19_pcr_test_management_system.Web.DTOs
{
    public class BookingReportDTO
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public TESTTYPE TestType { get; set; }
        public int BookingSlotId { get; set; }
        public BookingSlotDTO BookingSlot { get; set; }
        public BOOKINGSTATUS BookingStatus { get; set; }
    }
}
