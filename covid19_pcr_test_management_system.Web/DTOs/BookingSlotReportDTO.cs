﻿using covid19_pcr_test_management_system.Web.Enums;
using System;


namespace covid19_pcr_test_management_system.Web.DTOs
{
    public class BookingSlotReportDTO
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public int LocationId { get; set; }
        public LocationDTO Location { get; set; }
        public SLOTSTATUS Status { get; set; }
    }
}
