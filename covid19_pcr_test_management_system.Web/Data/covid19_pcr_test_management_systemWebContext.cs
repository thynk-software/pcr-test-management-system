﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using covid19_pcr_test_management_system.Web.DTOs;

namespace covid19_pcr_test_management_system.Web.Data
{
    public class covid19_pcr_test_management_systemWebContext : DbContext
    {
        public covid19_pcr_test_management_systemWebContext (DbContextOptions<covid19_pcr_test_management_systemWebContext> options)
            : base(options)
        {
        }

        public DbSet<covid19_pcr_test_management_system.Web.DTOs.BookingDTO> BookingDTO { get; set; }

        public DbSet<covid19_pcr_test_management_system.Web.DTOs.BookingSlotDTO> BookingSlotDTO { get; set; }

        public DbSet<covid19_pcr_test_management_system.Web.DTOs.LocationDTO> LocationDTO { get; set; }

        public DbSet<covid19_pcr_test_management_system.Web.DTOs.TestResultDTO> TestResultDTO { get; set; }
    }
}
