﻿using System;
using System.Collections.Generic;
using System.Text;

namespace covid19_pcr_test_management_system.Infrastructure.Enums
{
    public enum TESTSTATUS
    {
        POSITIVE,
        NEGATIVE
    }
}
