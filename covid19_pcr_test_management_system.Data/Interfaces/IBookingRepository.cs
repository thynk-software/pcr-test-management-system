﻿using covid19_pcr_test_management_system.Infrastructure.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace covid19_pcr_test_management_system.Infrastructure.Interfaces
{
    public interface IBookingRepository
    {
        Task<Booking> GetByIdAsync(int id);    
        Task<List<Booking>> ListAllAsync();        
        Task InsertAsync(Booking entity);
        Task UpdateAsync(Booking entity);
        Task DeleteAsync(int id);
    }
}
