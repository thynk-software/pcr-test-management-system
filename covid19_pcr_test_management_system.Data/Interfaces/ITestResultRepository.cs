﻿using covid19_pcr_test_management_system.Infrastructure.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace covid19_pcr_test_management_system.Infrastructure.Interfaces
{
    public interface ITestResultRepository
    {
        Task<TestResult> GetByIdAsync(int id);    
        Task<List<TestResult>> ListAllAsync();        
        Task InsertAsync(TestResult entity);
        Task UpdateAsync(TestResult entity);
        Task DeleteAsync(int id);
    }
}
