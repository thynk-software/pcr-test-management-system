﻿using covid19_pcr_test_management_system.Infrastructure.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace covid19_pcr_test_management_system.Infrastructure.Interfaces
{
    public interface ILocationRepository
    {
        Task<Location> GetByIdAsync(int id);    
        Task<List<Location>> ListAllAsync();        
        Task InsertAsync(Location entity);
        Task UpdateAsync(Location entity);
        Task DeleteAsync(int id);
    }
}
