﻿using covid19_pcr_test_management_system.Infrastructure.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace covid19_pcr_test_management_system.Infrastructure.Interfaces
{
    public interface IBookingSlotRepository
    {
        Task<BookingSlot> GetByIdAsync(int id);    
        Task<List<BookingSlot>> ListAllAsync();        
        Task InsertAsync(BookingSlot entity);
        Task UpdateAsync(BookingSlot entity);
        Task DeleteAsync(int id);
    }
}
