﻿using covid19_pcr_test_management_system.Infrastructure.Entities;
using covid19_pcr_test_management_system.Infrastructure.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace covid19_pcr_test_management_system.Infrastructure.Entities
{
    public class BookingSlot: BaseEntity
    {        
        public DateTime Date { get; set; }
        public int LocationId { get; set; }
        public SLOTSTATUS Status { get; set; }
    }
}
