﻿
using covid19_pcr_test_management_system.Infrastructure.Enums;

namespace covid19_pcr_test_management_system.Infrastructure.Entities
{
    public class Booking: BaseEntity
    {        
        public string FullName { get; set; }
        public string Phone { get; set; }
        public TESTTYPE TestType { get; set; }
        public int BookingSlotId { get; set; }
        public BOOKINGSTATUS BookingStatus { get; set; }
    }
}
