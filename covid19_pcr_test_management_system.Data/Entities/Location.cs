﻿namespace covid19_pcr_test_management_system.Infrastructure.Entities
{
    public class Location : BaseEntity
    {        
        public string Name { get; set; }
        public string Address { get; set; }
    }
}
