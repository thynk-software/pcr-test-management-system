﻿using covid19_pcr_test_management_system.Infrastructure.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace covid19_pcr_test_management_system.Infrastructure.Entities
{
    public class TestResult: BaseEntity
    {        
        public int BookingId { get; set; }
        public TESTSTATUS Status { get; set; }
        public string Remarks { get; set; }
    }
}
