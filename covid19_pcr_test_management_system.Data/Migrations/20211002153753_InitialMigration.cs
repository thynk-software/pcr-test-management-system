﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace covid19_pcr_test_management_system.Infrastructure.Migrations
{
    public partial class InitialMigration : Migration
    {        
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            createStoredProcedures(migrationBuilder);

            migrationBuilder.CreateTable(
                name: "Bookings",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FullName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Phone = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TestType = table.Column<int>(type: "int", nullable: false),
                    BookingSlotId = table.Column<int>(type: "int", nullable: false),
                    BookingStatus = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Bookings", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BookingSlots",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Date = table.Column<DateTime>(type: "datetime2", nullable: false),
                    LocationId = table.Column<int>(type: "int", nullable: false),
                    Status = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BookingSlots", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Locations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Address = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Locations", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TestResults",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BookingId = table.Column<int>(type: "int", nullable: false),
                    Status = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TestResults", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Bookings");

            migrationBuilder.DropTable(
                name: "BookingSlots");

            migrationBuilder.DropTable(
                name: "Locations");

            migrationBuilder.DropTable(
                name: "TestResults");
        }

        private void createStoredProcedures(MigrationBuilder migrationBuilder)
        {
            string createProcSql = "CREATE PROCEDURE spBookingSlot_GetById @Id int AS Select * FROM BookingSlots where Id = @Id";
            migrationBuilder.Sql(createProcSql);
            createProcSql = "CREATE PROCEDURE spLocation_GetById @Id int AS Select * FROM Locations where Id = @Id";
            migrationBuilder.Sql(createProcSql);
            createProcSql = "CREATE PROCEDURE spBooking_GetById @Id int AS Select * FROM Bookings where Id = @Id";
            migrationBuilder.Sql(createProcSql);
            createProcSql = "CREATE PROCEDURE spTestResult_GetById @Id int AS Select * FROM TestResults where Id = @Id";
            migrationBuilder.Sql(createProcSql);

            createProcSql = "CREATE PROCEDURE spBookingSlot_ListAll AS Select * FROM BookingSlots";
            migrationBuilder.Sql(createProcSql);
            createProcSql = "CREATE PROCEDURE spLocation_ListAll AS Select * FROM Locations";
            migrationBuilder.Sql(createProcSql);
            createProcSql = "CREATE PROCEDURE spBooking_ListAll AS Select * FROM Bookings";
            migrationBuilder.Sql(createProcSql);
            createProcSql = "CREATE PROCEDURE spTestResult_ListAll AS Select * FROM TestResults";
            migrationBuilder.Sql(createProcSql);

            createProcSql = "CREATE PROCEDURE spBookingSlot_Insert @Date datetime2(7), @LocationId int, @Status int AS insert into BookingSlots VALUES (@Date, @LocationId, @Status)";
            migrationBuilder.Sql(createProcSql);
            createProcSql = "CREATE PROCEDURE spLocation_Insert @Name varchar(256), @Address varchar(512) AS insert into Locations VALUES (@Name, @Address)";
            migrationBuilder.Sql(createProcSql);
            createProcSql = "CREATE PROCEDURE spBooking_Insert @FullName varchar(256), @Phone varchar(64), @TestType int, @BookingSlotId int, @BookingStatus int AS insert into Bookings VALUES (@FullName, @Phone, @TestType, @BookingSlotId, @BookingStatus)";
            migrationBuilder.Sql(createProcSql);
            createProcSql = "CREATE PROCEDURE spTestResult_Insert @BookingId int, @Status int, @Remarks varchar(MAX) AS insert into TestResults VALUES (@BookingId, @Status, @Remarks)";
            migrationBuilder.Sql(createProcSql);

            createProcSql = "CREATE PROCEDURE spBookingSlot_Update @Id int, @Date datetime2(7), @LocationId int, @Status int AS update BookingSlots SET Date = @Date, LocationId = @LocationId, Status = @Status Where Id = @Id";
            migrationBuilder.Sql(createProcSql);
            createProcSql = "CREATE PROCEDURE spLocation_Update @Id int, @Name varchar(256), @Address varchar(512) AS update Locations SET Name = @Name, Address = @Address Where Id = @Id";
            migrationBuilder.Sql(createProcSql);
            createProcSql = "CREATE PROCEDURE spBooking_Update @Id int, @FullName varchar(256), @Phone varchar(64), @TestType int, @BookingSlotId int, @BookingStatus int AS update Bookings SET FullName = @FullName, Phone = @Phone, TestType = @TestType, BookingSlotId = @BookingSlotId, BookingStatus = @BookingStatus Where Id = @Id";
            migrationBuilder.Sql(createProcSql);
            createProcSql = "CREATE PROCEDURE spTestResults_Update @Id int, @BookingId int, @Status int, @Remarks varchar(MAX) AS update TestResults SET BookingId = @BookingId, Status = @Status, Remarks = @Remarks Where Id = @Id";
            migrationBuilder.Sql(createProcSql);

            createProcSql = "CREATE PROCEDURE spBookingSlot_Delete @Id int AS delete BookingSlots Where Id = @Id";
            migrationBuilder.Sql(createProcSql);
            createProcSql = "CREATE PROCEDURE spLocation_Delete @Id int AS delete Locations Where Id = @Id";
            migrationBuilder.Sql(createProcSql);
            createProcSql = "CREATE PROCEDURE spBooking_Delete @Id int AS delete Bookings Where Id = @Id";
            migrationBuilder.Sql(createProcSql);
            createProcSql = "CREATE PROCEDURE spTestResults_Delete @Id int AS delete TestResults Where Id = @Id";
            migrationBuilder.Sql(createProcSql);
        }
    }
}
