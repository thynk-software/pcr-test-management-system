﻿using covid19_pcr_test_management_system.Infrastructure.Entities;
using covid19_pcr_test_management_system.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace covid19_pcr_test_management_system.Infrastructure.Data
{

    public class C19PCRTESTMSRepository<T> : IC19PCRTESTMSRepository<T> where T : BaseEntity
    {
        private readonly C19PCRTESTMSContext_Experimental<T> _dbContext;
        readonly DbSet<T> _dbSet;

        public C19PCRTESTMSRepository(C19PCRTESTMSContext_Experimental<T> dbContext)
        {
            _dbContext = dbContext;
            _dbSet = _dbContext.Set<T>();
        }

        public async Task<T> GetByIdAsync(int id)
        {
            //return await _dbContext.Set<T>().SingleOrDefaultAsync(e => e.Id == id);
            string sql = "EXEC SalesLT.Product_GetAll";
            return await _dbContext.Table.FromSqlRaw(sql).FirstOrDefaultAsync();
        }


        public async Task<List<T>> ListAllAsync()
        {
            string sql = "EXEC SalesLT.Product_GetAll";
            return await _dbContext.Table.FromSqlRaw(sql).ToListAsync();
        }


        public async Task InsertAsync(T entity)
        {
            string sql = "EXEC SalesLT.Product_GetAll";
            await Task.Run(() => _dbContext.Table.FromSqlRaw(sql));
        }

        public async Task UpdateAsync(T entity)
        {
            string sql = "EXEC SalesLT.Product_GetAll";
            await Task.Run(() => _dbContext.Table.FromSqlRaw(sql));
        }

        public async Task DeleteAsync(int id)
        {
            string sql = "EXEC SalesLT.Product_GetAll";
            await Task.Run(() => _dbContext.Table.FromSqlRaw(sql));            
        }
    }
    
}
