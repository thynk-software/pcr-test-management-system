﻿using covid19_pcr_test_management_system.Infrastructure.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace covid19_pcr_test_management_system.Infrastructure.Data
{
    public class AppDBContext: DbContext
    {
        public AppDBContext(DbContextOptions options)
           : base(options)
        {

        }     

        public DbSet<Location> Locations { get; set; }
        public DbSet<Booking> Bookings { get; set; }
        public DbSet<BookingSlot> BookingSlots { get; set; }
        public DbSet<TestResult> TestResults { get; set; }
    }
}
