﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore.Internal;
using covid19_pcr_test_management_system.Infrastructure.Entities;

namespace covid19_pcr_test_management_system.Infrastructure.Data
{
    public static class AppContextExtensions
    {
        public static void EnsureSeedDataForContext(this AppDBContext context)
        {
            if (context.Locations.Any())
            {
                return;
            }

            var locations = new List<Location>()
            {
                new Location()
                {
                     Name = "Lab A",
                  Address = "176, Constance Spring Road"
                },
               new Location()
                {
                     Name = "Lab B",
                  Address = "13, Carlisle Avenue"
                },
                 new Location()
                {
                     Name = "Lab C",
                  Address = "17A, Bush Street"
                }
            };

            context.Locations.AddRange(locations);
            context.SaveChanges();
        }
    }
}
