﻿using covid19_pcr_test_management_system.Infrastructure.Entities;
using covid19_pcr_test_management_system.Infrastructure.Interfaces;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace covid19_pcr_test_management_system.Infrastructure.Data
{

    public class LocationRepository : ILocationRepository
    {
        private readonly AppDBContext _dbContext;        

        public LocationRepository(AppDBContext dbContext)
        {
            _dbContext = dbContext;            
        }

        public async Task<Location> GetByIdAsync(int id)
        {      
            var parameters = new[] {
                 new SqlParameter("@Id", SqlDbType.Int) {Direction = ParameterDirection.Input, Value = id }
            };

            string sql = $"EXEC spLocation_GetById @Id";
            return await Task.Run(() => _dbContext.Locations.FromSqlRaw(sql, parameters).AsEnumerable().FirstOrDefault());
        }


        public async Task<List<Location>> ListAllAsync()
        {
            string sql = "EXEC spLocation_ListAll";
            return await _dbContext.Locations.FromSqlRaw(sql).ToListAsync();
        }


        public async Task InsertAsync(Location entity)
        {
            var parameters = new[] {
                 new SqlParameter("@Name", SqlDbType.VarChar) {Direction = ParameterDirection.Input, Value = entity.Name },
                  new SqlParameter("@Address", SqlDbType.VarChar) {Direction = ParameterDirection.Input, Value = entity.Address }                 
            };

            await _dbContext.Database.ExecuteSqlRawAsync("exec spLocation_Insert @Name, @Address", parameters: parameters);
        }

        public async Task UpdateAsync(Location entity)
        {     
            var parameters = new[] {
                new SqlParameter("@Id", SqlDbType.Int) {Direction = ParameterDirection.Input, Value = entity.Id },
                 new SqlParameter("@Name", SqlDbType.VarChar) {Direction = ParameterDirection.Input, Value = entity.Name },
                  new SqlParameter("@Address", SqlDbType.VarChar) {Direction = ParameterDirection.Input, Value = entity.Address }                 
            };

            await _dbContext.Database.ExecuteSqlRawAsync("exec spLocation_Update @Id, @Name, @Address", parameters: parameters);
        }

        public async Task DeleteAsync(int id)
        {          
            var parameters = new[] {new SqlParameter("@Id", SqlDbType.Int){Direction = ParameterDirection.Input, Value = id}
      };

            await _dbContext.Database.ExecuteSqlRawAsync("exec spLocation_Delete @Id", parameters: parameters);
        }
    }    
}
