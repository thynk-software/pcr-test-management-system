﻿using covid19_pcr_test_management_system.Infrastructure.Entities;
using covid19_pcr_test_management_system.Infrastructure.Interfaces;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace covid19_pcr_test_management_system.Infrastructure.Data
{

    public class BookingRepository : IBookingRepository
    {
        private readonly AppDBContext _dbContext;
      

        public BookingRepository(AppDBContext dbContext)
        {
            _dbContext = dbContext;            
        }

        public async Task<Booking> GetByIdAsync(int id)
        {
            var parameters = new[] {
                 new SqlParameter("@Id", SqlDbType.Int) {Direction = ParameterDirection.Input, Value = id }
            };

            string sql = $"EXEC spBooking_GetById @Id";
            return await Task.Run(() => _dbContext.Bookings.FromSqlRaw(sql, parameters).AsEnumerable().FirstOrDefault());
        }

        public async Task<List<Booking>> ListAllAsync()
        {
            string sql = "EXEC spBooking_ListAll";
            return await _dbContext.Bookings.FromSqlRaw(sql).ToListAsync();
        }

        public async Task InsertAsync(Booking entity)
        {
            var parameters = new[] {
                 new SqlParameter("@FullName", SqlDbType.VarChar) {Direction = ParameterDirection.Input, Value = entity.FullName },
                  new SqlParameter("@Phone", SqlDbType.VarChar) {Direction = ParameterDirection.Input, Value = entity.Phone },
                  new SqlParameter("@TestType", SqlDbType.Int) {Direction = ParameterDirection.Input, Value = Convert.ToInt32(entity.TestType) },
                  new SqlParameter("@BookingSlotId", SqlDbType.Int) {Direction = ParameterDirection.Input, Value = Convert.ToInt32(entity.TestType) },
                  new SqlParameter("@BookingStatus", SqlDbType.Int) {Direction = ParameterDirection.Input, Value = Convert.ToInt32(entity.BookingStatus) }
            };

            await _dbContext.Database.ExecuteSqlRawAsync("exec spBooking_Insert @FullName, @Phone, @TestType, @BookingSlotId, @BookingStatus", parameters: parameters);   
        }

        public async Task UpdateAsync(Booking entity)
        {
            var parameters = new[] {
                new SqlParameter("@Id", SqlDbType.Int) {Direction = ParameterDirection.Input, Value = entity.Id },
                 new SqlParameter("@FullName", SqlDbType.VarChar) {Direction = ParameterDirection.Input, Value = entity.FullName },
                  new SqlParameter("@Phone", SqlDbType.VarChar) {Direction = ParameterDirection.Input, Value = entity.Phone },
                  new SqlParameter("@TestType", SqlDbType.Int) {Direction = ParameterDirection.Input, Value = Convert.ToInt32(entity.TestType) },
                  new SqlParameter("@BookingSlotId", SqlDbType.Int) {Direction = ParameterDirection.Input, Value = entity.BookingSlotId }
            };

            await _dbContext.Database.ExecuteSqlRawAsync("exec spBooking_Update @Id, @FullName, @Phone, @TestType, @BookingSlotId", parameters: parameters);         
        }

        public async Task DeleteAsync(int id)
        {         
            var parameters = new[] {new SqlParameter("@Id", SqlDbType.Int){Direction = ParameterDirection.Input, Value = id}
      };

            await _dbContext.Database.ExecuteSqlRawAsync("exec spBooking_Delete @Id", parameters: parameters);
        }
    }    
}
