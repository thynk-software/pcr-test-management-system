﻿using covid19_pcr_test_management_system.Infrastructure.Entities;
using covid19_pcr_test_management_system.Infrastructure.Interfaces;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace covid19_pcr_test_management_system.Infrastructure.Data
{

    public class TestResultRepository : ITestResultRepository
    {
        private readonly AppDBContext _dbContext;        

        public TestResultRepository(AppDBContext dbContext)
        {
            _dbContext = dbContext;            
        }

        public async Task<TestResult> GetByIdAsync(int id)
        {
            var parameters = new[] {
                 new SqlParameter("@Id", SqlDbType.Int) {Direction = ParameterDirection.Input, Value = id }
            };

            string sql = $"EXEC spTestResult_GetById @Id";
            return await Task.Run(() => _dbContext.TestResults.FromSqlRaw(sql, parameters).AsEnumerable().FirstOrDefault());
        }


        public async Task<List<TestResult>> ListAllAsync()
        {
            string sql = "EXEC spTestResult_ListAll";
            return await _dbContext.TestResults.FromSqlRaw(sql).ToListAsync();
        }


        public async Task InsertAsync(TestResult entity)
        {       
            var parameters = new[] {
                 new SqlParameter("@BookingId", SqlDbType.Int) {Direction = ParameterDirection.Input, Value = entity.BookingId },
                  new SqlParameter("@Status", SqlDbType.Int) {Direction = ParameterDirection.Input, Value = entity.Status },
                  new SqlParameter("@Remarks", SqlDbType.VarChar) {Direction = ParameterDirection.Input, Value = entity.Remarks }
            };

            await _dbContext.Database.ExecuteSqlRawAsync("exec spTestResult_Insert @BookingId, @Status, @Remarks", parameters: parameters);
        }

        public async Task UpdateAsync(TestResult entity)
        {
            var parameters = new[] {
                new SqlParameter("@Id", SqlDbType.Int) {Direction = ParameterDirection.Input, Value = entity.Id },
                 new SqlParameter("@BookingId", SqlDbType.Int) {Direction = ParameterDirection.Input, Value = entity.BookingId },
                  new SqlParameter("@Status", SqlDbType.Int) {Direction = ParameterDirection.Input, Value = Convert.ToInt32(entity.Status) },
                  new SqlParameter("@Remarks", SqlDbType.VarChar) {Direction = ParameterDirection.Input, Value = entity.Remarks }
            };

            await _dbContext.Database.ExecuteSqlRawAsync("exec spTestResults_Update @Id, @BookingId, @Status, @Remarks", parameters: parameters);
        }

        public async Task DeleteAsync(int id)
        {      
            var parameters = new[] {new SqlParameter("@Id", SqlDbType.Int){Direction = ParameterDirection.Input, Value = id}
      };

            await _dbContext.Database.ExecuteSqlRawAsync("exec spTestResults_Delete @Id", parameters: parameters);
        }
    }
    
}
