﻿using covid19_pcr_test_management_system.Infrastructure.Entities;
using covid19_pcr_test_management_system.Infrastructure.Interfaces;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace covid19_pcr_test_management_system.Infrastructure.Data
{

    public class BookingSlotRepository : IBookingSlotRepository
    {
        private readonly AppDBContext _dbContext;        

        public BookingSlotRepository(AppDBContext dbContext)
        {
            _dbContext = dbContext;            
        }

        public async Task<BookingSlot> GetByIdAsync(int id)
        {
            var parameters = new[] {
                 new SqlParameter("@Id", SqlDbType.Int) {Direction = ParameterDirection.Input, Value = id }
            };

            string sql = $"EXEC spBookingSlot_GetById @Id";
            return await Task.Run(() => _dbContext.BookingSlots.FromSqlRaw(sql, parameters).AsEnumerable().FirstOrDefault());
        }


        public async Task<List<BookingSlot>> ListAllAsync()
        {
            string sql = "EXEC spBookingSlot_ListAll";
            return await _dbContext.BookingSlots.FromSqlRaw(sql).ToListAsync();
        }

        public async Task InsertAsync(BookingSlot entity)
        {         
            var parameters = new[] {
                 new SqlParameter("@Date", SqlDbType.DateTime2) {Direction = ParameterDirection.Input, Value = entity.Date },
                  new SqlParameter("@LocationId", SqlDbType.Int) {Direction = ParameterDirection.Input, Value = entity.LocationId },
                  new SqlParameter("@Status", SqlDbType.Int) {Direction = ParameterDirection.Input, Value = Convert.ToInt32(entity.Status) }
            };

            await _dbContext.Database.ExecuteSqlRawAsync("exec spBookingSlot_Insert @Date, @LocationId, @Status", parameters: parameters);
        }

        public async Task UpdateAsync(BookingSlot entity)
        {            
            var parameters = new[] {
                new SqlParameter("@Id", SqlDbType.Int) {Direction = ParameterDirection.Input, Value = entity.Id },
                 new SqlParameter("@Date", SqlDbType.DateTime2) {Direction = ParameterDirection.Input, Value = entity.Date },
                  new SqlParameter("@LocationId", SqlDbType.Int) {Direction = ParameterDirection.Input, Value = entity.LocationId },
                  new SqlParameter("@Status", SqlDbType.Int) {Direction = ParameterDirection.Input, Value = Convert.ToInt32(entity.Status) }
            };

            await _dbContext.Database.ExecuteSqlRawAsync("exec spBookingSlot_Update @Id, @Date, @LocationId, @Status", parameters: parameters);
        }

        public async Task DeleteAsync(int id)
        {
            var parameters = new[] {new SqlParameter("@Id", SqlDbType.Int){Direction = ParameterDirection.Input, Value = id}
      };

          await _dbContext.Database.ExecuteSqlRawAsync("exec spBookingSlot_Delete @Id", parameters: parameters);
        }
    }
    
}
