﻿using covid19_pcr_test_management_system.Infrastructure.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace covid19_pcr_test_management_system.Infrastructure.Data
{
    public class C19PCRTESTMSContext_Experimental<T>: DbContext where T: BaseEntity
    {
        public C19PCRTESTMSContext_Experimental(DbContextOptions<C19PCRTESTMSContext_Experimental<T>> options)
           : base(options)
        {

        }

        public DbSet<T> Table { get; set; }
    //    public DbSet<Booking> Bookings { get; set; }
        //public DbSet<BookingSlot> BookingSlots { get; set; }
        //public DbSet<TestResult> TestResults { get; set; }
    }
}
