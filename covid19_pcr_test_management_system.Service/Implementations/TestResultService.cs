﻿using AutoMapper;
using covid19_pcr_test_management_system.Infrastructure.Entities;
using covid19_pcr_test_management_system.Infrastructure.Interfaces;
using covid19_pcr_test_management_system.Service.DTOs;
using covid19_pcr_test_management_system.Service.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace covid19_pcr_test_management_system.Service.Implementations
{
    public class TestResultService : ITestResultService
    {
        private readonly ITestResultRepository _testResultRepository;
        private readonly IBookingRepository _bookingRepository;
        private readonly IBookingSlotRepository _bookingSlotRepository;
        private readonly IMapper _mapper;

        public TestResultService(ITestResultRepository testResultRepository, IMapper mapper)
        {
            _testResultRepository = testResultRepository;
            _mapper = mapper;
        }

        public async Task DeleteAsync(int id)
        {
            await _testResultRepository.DeleteAsync(id);
        }

        public async Task<TestResultDTO> GetByIdAsync(int id)
        {
            var testResult = await _testResultRepository.GetByIdAsync(id);
            return _mapper.Map<TestResultDTO>(testResult);
        }

        public async Task InsertAsync(TestResultDTO entity)
        {            
            //set test result
            var testResult = _mapper.Map<TestResult>(entity);
            await _testResultRepository.InsertAsync(testResult);

            //set booking to completed
            var booking = await _bookingRepository.GetByIdAsync(entity.BookingId);
            booking.BookingStatus = Infrastructure.Enums.BOOKINGSTATUS.COMPLETED;

            //release booking slot
            var slot = await _bookingSlotRepository.GetByIdAsync(booking.BookingSlotId);
            slot.Status = Infrastructure.Enums.SLOTSTATUS.AVAILABLE;
            await _bookingSlotRepository.UpdateAsync(slot);
        }

        public async Task<List<TestResultDTO>> ListAllAsync()
        {
            var testResult = await _testResultRepository.ListAllAsync();
            return _mapper.Map<List<TestResultDTO>>(testResult);
        }

        public async Task UpdateAsync(TestResultDTO entity)
        {
            var testResult = _mapper.Map<TestResult>(entity);
            await _testResultRepository.UpdateAsync(testResult);
        }
    }
}
