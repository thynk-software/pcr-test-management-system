﻿using AutoMapper;
using covid19_pcr_test_management_system.Infrastructure.Entities;
using covid19_pcr_test_management_system.Infrastructure.Interfaces;
using covid19_pcr_test_management_system.Service.DTOs;
using covid19_pcr_test_management_system.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace covid19_pcr_test_management_system.Service.Implementations
{
    public class BookingService : IBookingService
    {
        private readonly IBookingRepository _bookingRepository;
        private readonly IBookingSlotRepository _bookingSlotRepository;
        private readonly IMapper _mapper;

        public BookingService(IBookingRepository BookingRepository, IBookingSlotRepository bookingSlotRepository, IMapper mapper)
        {
            _bookingRepository = BookingRepository;
            _bookingSlotRepository = bookingSlotRepository;
            _mapper = mapper;
        }

        public async Task DeleteAsync(int id)
        {
            var booking = await _bookingRepository.GetByIdAsync(id);
            //delete booking
            await _bookingRepository.DeleteAsync(id);

            //set slot to available and update         
            var slot = await _bookingSlotRepository.GetByIdAsync(booking.BookingSlotId);
            slot.Status = Infrastructure.Enums.SLOTSTATUS.AVAILABLE;
            await _bookingSlotRepository.UpdateAsync(slot);
        }

        public async Task<BookingDTO> GetByIdAsync(int id)
        {
            var Booking =  await _bookingRepository.GetByIdAsync(id);
            return _mapper.Map<BookingDTO>(Booking);             
        }

        public async Task InsertAsync(BookingDTO entity)
        {
            //check availability of slot
            var availableSlot = await _bookingSlotRepository.GetByIdAsync(entity.BookingSlotId);
            if (availableSlot == null || availableSlot?.Status == Infrastructure.Enums.SLOTSTATUS.TAKEN)
            {
                throw new Exception("Selected slot is not available");
            }

            //set booking status to booked and insert
            entity.BookingStatus = Infrastructure.Enums.BOOKINGSTATUS.BOOKED;
            var booking = _mapper.Map<Booking>(entity);            
            await _bookingRepository.InsertAsync(booking);

            //set slot to taken and update
            availableSlot.Status = Infrastructure.Enums.SLOTSTATUS.TAKEN;
            await _bookingSlotRepository.UpdateAsync(availableSlot);
        }

        public async Task<List<BookingDTO>> ListAllAsync()
        {
            var booking = await _bookingRepository.ListAllAsync();
            return _mapper.Map<List<BookingDTO>>(booking);
        }

        public async Task UpdateAsync(BookingDTO entity)
        {
            var booking = _mapper.Map<Booking>(entity);
            await _bookingRepository.UpdateAsync(booking);
        }
    }
}
