﻿using AutoMapper;
using covid19_pcr_test_management_system.Infrastructure.Entities;
using covid19_pcr_test_management_system.Infrastructure.Interfaces;
using covid19_pcr_test_management_system.Service.DTOs;
using covid19_pcr_test_management_system.Service.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace covid19_pcr_test_management_system.Service.Implementations
{
    public class LocationService : ILocationService
    {
        private readonly ILocationRepository _locationRepository;
        private readonly IMapper _mapper;

        public LocationService(ILocationRepository locationRepository, IMapper mapper)
        {
            _locationRepository = locationRepository;
            _mapper = mapper;
        }

        public async Task DeleteAsync(int id)
        {
            await _locationRepository.DeleteAsync(id);
        }

        public async Task<LocationDTO> GetByIdAsync(int id)
        {
            var location =  await _locationRepository.GetByIdAsync(id);
            return _mapper.Map<LocationDTO>(location);             
        }

        public async Task InsertAsync(LocationDTO entity)
        {
            var existingLocationList = await _locationRepository.ListAllAsync();
            if (existingLocationList.Where(x => x.Name == entity.Name).Count() > 0)
            {
                return;
                //throw new Exception("Slot Already Exists");
            }

            var location = _mapper.Map<Location>(entity);
            await _locationRepository.InsertAsync(location);
        }

        public async Task<List<LocationDTO>> ListAllAsync()
        {
            var location = await _locationRepository.ListAllAsync();
            return _mapper.Map<List<LocationDTO>>(location);
        }

        public async Task UpdateAsync(LocationDTO entity)
        {
            var location = _mapper.Map<Location>(entity);
            await _locationRepository.UpdateAsync(location);
        }
    }
}
