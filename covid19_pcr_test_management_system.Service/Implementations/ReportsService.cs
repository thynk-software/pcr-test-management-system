﻿using AutoMapper;
using covid19_pcr_test_management_system.Infrastructure.Entities;
using covid19_pcr_test_management_system.Infrastructure.Interfaces;
using covid19_pcr_test_management_system.Service.DTOs;
using covid19_pcr_test_management_system.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace covid19_pcr_test_management_system.Service.Implementations
{
    public class ReportsService : IReportsService
    {
        private readonly IBookingRepository _bookingRepository;
        private readonly IBookingSlotRepository _bookingSlotRepository;
        private readonly ITestResultRepository _testResultRepository;
        private readonly ILocationRepository _locationRepository;
        private readonly IMapper _mapper;

        public ReportsService(IBookingRepository BookingRepository, IBookingSlotRepository bookingSlotRepository,
            ITestResultRepository testResultRepository, ILocationRepository locationRepository, IMapper mapper)
        {
            _bookingRepository = BookingRepository;
            _bookingSlotRepository = bookingSlotRepository;
            _testResultRepository = testResultRepository;
            _locationRepository = locationRepository;
            _mapper = mapper;
        }
  

        public async Task<List<BookingReportDTO>> BookingReport()
        {
            var booking = await _bookingRepository.ListAllAsync();
            var bookingReportDTOList = _mapper.Map<List<BookingReportDTO>>(booking);
            bookingReportDTOList.ForEach(x => x.BookingSlot = 
            _mapper.Map<BookingSlotDTO>(_bookingSlotRepository.GetByIdAsync(x.BookingSlotId).Result));

            return bookingReportDTOList;
        }

        public async Task<List<BookingSlotReportDTO>> BookingSlotReport()
        {
            var bookingSlot = await _bookingSlotRepository.ListAllAsync();
            var bookingSlotReportDTOList = _mapper.Map<List<BookingSlotReportDTO>>(bookingSlot);
            bookingSlotReportDTOList.ForEach(x=> x.Location =
            _mapper.Map<LocationDTO>(_locationRepository.GetByIdAsync(x.LocationId).Result));

            return bookingSlotReportDTOList;
        }

        public async Task<List<TestReportDTO>> TestReport()
        {
            var testResult = await _testResultRepository.ListAllAsync();
            var testReportDTOList = _mapper.Map<List<TestReportDTO>>(testResult);
            testReportDTOList.ForEach(x => x.Booking = BookingReport().Result.Where(y=> y.Id == x.BookingId).FirstOrDefault());

            return testReportDTOList;
        }
    }
}
