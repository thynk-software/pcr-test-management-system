﻿using AutoMapper;
using covid19_pcr_test_management_system.Infrastructure.Entities;
using covid19_pcr_test_management_system.Infrastructure.Interfaces;
using covid19_pcr_test_management_system.Service.DTOs;
using covid19_pcr_test_management_system.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace covid19_pcr_test_management_system.Service.Implementations
{
    public class BookingSlotService : IBookingSlotService
    {
        private readonly IBookingSlotRepository _bookingSlotRepository;
        private readonly IMapper _mapper;

        public BookingSlotService(IBookingSlotRepository bookingSlotRepository, IMapper mapper)
        {
            _bookingSlotRepository = bookingSlotRepository;
            _mapper = mapper;
        }       

        public async Task<BookingSlotDTO> GetByIdAsync(int id)
        {
            var bookingSlot = await _bookingSlotRepository.GetByIdAsync(id);
            return _mapper.Map<BookingSlotDTO>(bookingSlot);
        }  

        public async Task<List<BookingSlotDTO>> ListAllAsync()
        {
            var bookingSlot = await _bookingSlotRepository.ListAllAsync();
           //return  bookingSlot == null ? null : _mapper.Map<List<BookingSlotDTO>>(bookingSlot);
           return _mapper.Map<List<BookingSlotDTO>>(bookingSlot);
        }

        public async Task InsertAsync(BookingSlotDTO entity)
        {
            var existingSlotsList = await _bookingSlotRepository.ListAllAsync();
            if (existingSlotsList.Where(x => x.Date.Date == entity.Date.Date && x.LocationId == entity.LocationId).Count() > 0)
            {
                return;
                //throw new Exception("Slot Already Exists");
            }

            var booking = _mapper.Map<BookingSlot>(entity);
            await _bookingSlotRepository.InsertAsync(booking);
        }

        public async Task UpdateAsync(BookingSlotDTO entity)
        {
            var location = _mapper.Map<BookingSlot>(entity);
            await _bookingSlotRepository.UpdateAsync(location);
        }

        public async Task DeleteAsync(int id)
        {
            await _bookingSlotRepository.DeleteAsync(id);
        }
    }
}
