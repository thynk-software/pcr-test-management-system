﻿using covid19_pcr_test_management_system.Infrastructure.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace covid19_pcr_test_management_system.Service.DTOs
{
    public class TestReportDTO
    {
        public int Id { get; set; }
        public int BookingId { get; set; }
        public BookingReportDTO Booking { get; set; }
        public TESTSTATUS Status { get; set; }
    }
}
