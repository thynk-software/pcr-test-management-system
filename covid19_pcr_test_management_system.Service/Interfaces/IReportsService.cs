﻿using covid19_pcr_test_management_system.Service.DTOs;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace covid19_pcr_test_management_system.Service.Interfaces
{
    public interface IReportsService
    {       
        Task<List<BookingReportDTO>> BookingReport();
        Task<List<BookingSlotReportDTO>> BookingSlotReport();
        Task<List<TestReportDTO>> TestReport();
    }
}
