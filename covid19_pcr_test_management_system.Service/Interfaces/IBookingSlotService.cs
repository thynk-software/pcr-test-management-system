﻿using covid19_pcr_test_management_system.Service.DTOs;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace covid19_pcr_test_management_system.Service.Interfaces
{
    public interface IBookingSlotService
    {
        Task<BookingSlotDTO> GetByIdAsync(int id);
        Task<List<BookingSlotDTO>> ListAllAsync();
        Task InsertAsync(BookingSlotDTO entity);
        Task UpdateAsync(BookingSlotDTO entity);
        Task DeleteAsync(int id);
    }
}
