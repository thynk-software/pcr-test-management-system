﻿using covid19_pcr_test_management_system.Service.DTOs;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace covid19_pcr_test_management_system.Service.Interfaces
{
    public interface IBookingService
    {
        Task<BookingDTO> GetByIdAsync(int id);
        Task<List<BookingDTO>> ListAllAsync();
        Task InsertAsync(BookingDTO entity);
        Task UpdateAsync(BookingDTO entity);
        Task DeleteAsync(int id);
    }
}
