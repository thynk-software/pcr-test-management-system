﻿using covid19_pcr_test_management_system.Service.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace covid19_pcr_test_management_system.Service.Interfaces
{
    public interface ITestResultService
    {
        Task<TestResultDTO> GetByIdAsync(int id);
        Task<List<TestResultDTO>> ListAllAsync();
        Task InsertAsync(TestResultDTO entity);
        Task UpdateAsync(TestResultDTO entity);
        Task DeleteAsync(int id);
    }
}
