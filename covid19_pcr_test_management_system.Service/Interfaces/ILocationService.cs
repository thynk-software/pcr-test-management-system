﻿using covid19_pcr_test_management_system.Service.DTOs;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace covid19_pcr_test_management_system.Service.Interfaces
{
    public interface ILocationService
    {
        Task<LocationDTO> GetByIdAsync(int id);
        Task<List<LocationDTO>> ListAllAsync();
        Task InsertAsync(LocationDTO entity);
        Task UpdateAsync(LocationDTO entity);
        Task DeleteAsync(int id);
    }
}
