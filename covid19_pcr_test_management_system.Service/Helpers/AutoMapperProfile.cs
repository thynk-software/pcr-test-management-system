using AutoMapper;
using covid19_pcr_test_management_system.Infrastructure.Entities;
using covid19_pcr_test_management_system.Service.DTOs;

namespace covid19_pcr_test_management_system.Service.Helpers
{
    public class AutoMapperProfile : Profile
    {
        // mappings between model and entity objects
        public AutoMapperProfile()
        {
            CreateMap<LocationDTO, Location>().ReverseMap();
            CreateMap<BookingDTO, Booking>().ReverseMap();
            CreateMap<Booking, BookingReportDTO>();
            CreateMap<BookingSlotDTO, BookingSlot>().ReverseMap();
            CreateMap<BookingSlot, BookingSlotReportDTO>();
            CreateMap<TestResultDTO, TestResult>().ReverseMap();
            CreateMap<TestResult, TestReportDTO>();
        }        
    }
}