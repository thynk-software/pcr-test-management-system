using covid19_pcr_test_management_system.Infrastructure.Data;
using covid19_pcr_test_management_system.Infrastructure.Interfaces;
using covid19_pcr_test_management_system.Service.Implementations;
using covid19_pcr_test_management_system.Service.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System;

namespace covid19_pcr_test_management_system
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<DbContext, AppDBContext>();
            services.AddTransient<ILocationRepository, LocationRepository>();
            services.AddTransient<ITestResultRepository, TestResultRepository>();
            services.AddTransient<IBookingRepository, BookingRepository>();
            services.AddTransient<IBookingSlotRepository, BookingSlotRepository>();

            services.AddTransient<IBookingSlotService, BookingSlotService>();
            services.AddTransient<IBookingService, BookingService>();            
            services.AddTransient<ILocationService, LocationService>();
            services.AddTransient<ITestResultService, TestResultService>();
            services.AddTransient<IReportsService, ReportsService>();

            var connectionString = Configuration["connectionStrings:covid_pcr_ConnectionString"];
            services.AddDbContext<AppDBContext>(o => o.UseSqlServer(connectionString));

            services.AddControllers();
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "covid19_pcr_test_management_system", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, AppDBContext context)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "covid19_pcr_test_management_system v1"));
            }

            app.UseRouting();

            app.UseAuthorization();

            context.Database.Migrate();
            context.EnsureSeedDataForContext();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
