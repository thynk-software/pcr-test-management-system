﻿using covid19_pcr_test_management_system.Service.DTOs;
using covid19_pcr_test_management_system.Service.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace covid19_pcr_test_management_system.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BookingController : ControllerBase
    {
        readonly IBookingService _bookingService;
        readonly IBookingSlotService _bookingSlotService;

        private readonly ILogger<BookingController> _logger;

        public BookingController(IBookingService bookingService, IBookingSlotService bookingSlotService)
        {
            _bookingService = bookingService;
            _bookingSlotService = bookingSlotService;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<BookingDTO>>> Get(int id)
        {
            try
            {
                BookingDTO booking = await _bookingService.GetByIdAsync(id);
                return Ok(booking);
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<BookingDTO>>> Get()
        {
            try
            {
                return Ok(await _bookingService.ListAllAsync());
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }       
        
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] BookingDTO booking)
        {
            try
            {                            
                await _bookingService.InsertAsync(booking);         
                return Created("", "");
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }
        
        [HttpPut]
        public async Task<ActionResult> Put([FromBody] BookingDTO booking)
        {
            try
            {
                await _bookingService.UpdateAsync(booking);
                return Created("", "");
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }
        
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                await _bookingService.DeleteAsync(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }
    }
}
