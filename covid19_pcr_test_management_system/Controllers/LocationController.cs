﻿using covid19_pcr_test_management_system.Service.DTOs;
using covid19_pcr_test_management_system.Service.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace covid19_pcr_test_management_system.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class LocationController : ControllerBase
    {
        readonly ILocationService _locationService;

        private readonly ILogger<LocationController> _logger;

        public LocationController(ILocationService LocationService)
        {
            _locationService = LocationService;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<LocationDTO>>> Get(int id)
        {
            try
            {
                LocationDTO Location = await _locationService.GetByIdAsync(id);
                return Ok(Location);
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<LocationDTO>>> Get()
        {
            try
            {
                return Ok(await _locationService.ListAllAsync());
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpPost]
        public async Task<ActionResult> Post([FromBody] LocationDTO Location)
        {
            try
            {
                await _locationService.InsertAsync(Location);
                return Created("", "");
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpPut]
        public async Task<ActionResult> Put([FromBody] LocationDTO booking)
        {
            try
            {
                await _locationService.UpdateAsync(booking);
                return Created("", "");
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                await _locationService.DeleteAsync(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }
    }
}
