﻿using covid19_pcr_test_management_system.Service.DTOs;
using covid19_pcr_test_management_system.Service.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace covid19_pcr_test_management_system.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ReportsController : ControllerBase
    {
        readonly IReportsService _reportsService;        
        private readonly ILogger<BookingController> _logger;

        public ReportsController(IReportsService reportsService)
        {
            _reportsService = reportsService;
        }

       
        [HttpGet("BookingReport")]
        public async Task<ActionResult<IEnumerable<BookingReportDTO>>> BookingReport()
        {
            try
            {
                return Ok(await _reportsService.BookingReport());
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }


        [HttpGet("BookingSlotReport")]
        public async Task<ActionResult<IEnumerable<BookingSlotReportDTO>>> BookingSlotReport()
        {
            try
            {
                return Ok(await _reportsService.BookingSlotReport());
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }


        [HttpGet("TestReport")]
        public async Task<ActionResult<IEnumerable<TestReportDTO>>> TestReport()
        {
            try
            {
                return Ok(await _reportsService.TestReport());
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }
    }
}
