﻿using covid19_pcr_test_management_system.Service.DTOs;
using covid19_pcr_test_management_system.Service.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace covid19_pcr_test_management_system.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BookingSlotController : ControllerBase
    {
        readonly IBookingSlotService _bookingSlotService;

        private readonly ILogger<BookingSlotController> _logger;

        public BookingSlotController(IBookingSlotService bookingSlotService)
        {
            _bookingSlotService = bookingSlotService;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<BookingSlotDTO>>> Get(int id)
        {
            try
            {
                BookingSlotDTO bookingSlot = await _bookingSlotService.GetByIdAsync(id);
                return Ok(bookingSlot);
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<BookingSlotDTO>>> Get()
        {
            try
            {
                return Ok(await _bookingSlotService.ListAllAsync());
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpPost]
        public async Task<ActionResult> Post([FromBody] BookingSlotDTO bookingSlot)
        {
            try
            {                           
                await _bookingSlotService.InsertAsync(bookingSlot);
                return Created("", "");
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpPut]
        public async Task<ActionResult> Put([FromBody] BookingSlotDTO booking)
        {
            try
            {
                await _bookingSlotService.UpdateAsync(booking);
                return Created("", "");
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                await _bookingSlotService.DeleteAsync(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }
    }
}
