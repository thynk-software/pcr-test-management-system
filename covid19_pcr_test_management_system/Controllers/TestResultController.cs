﻿using covid19_pcr_test_management_system.Service.DTOs;
using covid19_pcr_test_management_system.Service.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace covid19_pcr_test_management_system.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TestResultController : ControllerBase
    {
        readonly ITestResultService _testResultService;

        private readonly ILogger<TestResultController> _logger;

        public TestResultController(ITestResultService testResultService)
        {
            _testResultService = testResultService;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<TestResultDTO>>> Get(int id)
        {
            try
            {
                TestResultDTO TestResult = await _testResultService.GetByIdAsync(id);
                return Ok(TestResult);
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<TestResultDTO>>> Get()
        {
            try
            {
                return Ok(await _testResultService.ListAllAsync());
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpPost]
        public async Task<ActionResult> Post([FromBody] TestResultDTO TestResult)
        {
            try
            {
                await _testResultService.InsertAsync(TestResult);
                return Created("", "");
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpPut]
        public async Task<ActionResult> Put([FromBody] TestResultDTO booking)
        {
            try
            {
                await _testResultService.UpdateAsync(booking);
                return Created("", "");
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                await _testResultService.DeleteAsync(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }
    }
}
